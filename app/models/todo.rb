class Todo < ApplicationRecord
    
    # override in the case the json is empty
    def todo_items
        if self[:todo_items]
            self[:todo_items]
        else 
            '[]'
        end
    end
end
