import Vue from 'vue/dist/vue.esm'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        screenValue: "",
        currentOperator: "",
        computedValue: "",
        finalValueShowing: false,
        history: [],
        currentHistoryActive: -1,
    },

    getters: {
        isLatestHistory (state) {
            return state.history.length - 1 == state.currentHistoryActive
        },

        isFirstHistory (state) {
            return state.currentHistoryActive == 0
        },
    },

    mutations: {
        setScreen (state, value) {
            state.screenValue = value
        },
        
        setOperator (state, value) {
            state.currentOperator = value
        },

        setComputed (state, value) {
            state.computedValue = value
        },

        setFinalFlag (state, value) {
            state.finalValueShowing = value
        },

        removeOldFuture(state) {
            state.history.splice(state.currentHistoryActive + 1)
        },

        pushToHistory(state, value) {
            state.history.push(value)
        },

        setHistoryActive(state, value) {
            state.currentHistoryActive = value
        },
    },

    actions: {
        saveStateAsHistory({commit, state}) {
            const historyItem = {
                screenValue: state.screenValue,
                currentOperator: state.currentOperator,
                computedValue: state.computedValue,
                finalValueShowing: state.finalValueShowing,
            };

            commit('pushToHistory', historyItem)
            commit('setHistoryActive', state.history.length - 1)
        },
    },
})