// Do I need to include other files here to pipe through Babel?
import Vue from 'vue/dist/vue.esm';
import Calculator from '../calculator/Calculator';
import store from '../calculator/Store';

document.addEventListener('DOMContentLoaded', () => {
    Vue.component('simple-calculator', Calculator);

    const vm = new Vue ({
        store,
        el: '#root',
    })
});