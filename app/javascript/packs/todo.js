// todo is this really the best place to do this? Do all JS assets have to go in this folder?
// documentation is very spotty.

// Do I need to include other files here to pipe through Babel?
import Vue from 'vue/dist/vue.esm';
// import Vue from 'vue';
import App from '../todo/App';

document.addEventListener('DOMContentLoaded', () => {
    Vue.component('todo-items', App);

    const vm = new Vue ({
        el: '#root',
    })

    

    // Does not work with the slots.
    // new Vue({
    //     render: h => h(App)
    // }).$mount('#root');
});