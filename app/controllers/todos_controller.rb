class TodosController < ApplicationController

    # todo change to show all lists
    def index
        # move to show and grab by ID
        @todos = Todo.all
    end

    def edit
        @todo = Todo.find(params[:id])

        if (@todo)
            # todo check if this needs a default value
            render :edit
        else
            redirect_to todos_path
        end
    end

    def new
        @todo = Todo.new
    end

    # todo double check this
    def create
        @todo = Todo.new(todo_attributes)

        if @todo.save
            redirect_to(edit_todo_path(id: @todo.id))
        else
            # todo add error messages
            render('new')
        end
    end

    # Ajax Only For Now
    def update 
        # todo handle case where todo doesn't exist
        todo = Todo.find(params[:id])
        todo.todo_items = params[:data];
        todo.save

        render json: params[:data]
    end

    def destroy
        todo = Todo.find(params[:id])
        todo.destroy

        redirect_to(todos_path)
    end

    private

    def todo_attributes
        params.require(:todo).permit(:name)
    end

end
