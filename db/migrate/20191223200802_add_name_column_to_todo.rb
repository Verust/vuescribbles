class AddNameColumnToTodo < ActiveRecord::Migration[6.0]
  def up
    add_column('todos', 'name', 'string')
  end

  def down
    remove_column('todos', 'name')
  end
end
