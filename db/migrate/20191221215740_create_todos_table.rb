class CreateTodosTable < ActiveRecord::Migration[6.0]
  def change
    create_table :todos do |t|
      t.json "todo_items"

      t.timestamps
    end
  end
end
