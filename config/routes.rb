Rails.application.routes.draw do
  root 'main#index'
  
  get 'calculator', to: 'main#calculator'

  resources :todos, except: [:show]
end
